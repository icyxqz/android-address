# Android通讯录

项目忘记了之前是再那里找的一个，然后整体改成了基本和mvc类似的架构方式。引入了百度的语音识别技术。

#### 介绍
数据库：SQLite

开发工具：Androidstudio

1. 添加了百度在线语音识别
2. 基本增删查改实现
3. 能导入到手机电话簿

# 效果图

![](https://pic.downk.cc/item/5ff1a46c3ffa7d37b3a97d93.gif)