package com.yq.txl.dao;

import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.yq.txl.model.User;
import com.yq.txl.util.MyDB;

/**
 * 联系人表操作
 */
public class ContactsTable {

	private   final static String TABLENAME="contactsTable";//表名
	private MyDB db;//数据库管理
	public    ContactsTable(Context context)
	{
	    db=new  MyDB(context);
	    if(!db.isTableExits(TABLENAME))
		{
		     String   createTableSql="CREATE TABLE IF NOT EXISTS " +
		     TABLENAME + " (  id_DB  integer   " +
		     "primary key  AUTOINCREMENT , " +
		     User.NAME     + "  VARCHAR," +
		     User.MOBLIE    + "  VARCHAR,"+
		     User.QQ    + "  VARCHAR,"+
		     User.DANWEI    + "  VARCHAR,"+
		     User.ADDRESS+ " VARCHAR)";

		     //创建表
             db.creatTable(createTableSql);
		}
	}
	/**
	 * 添加数据到联系人表
	 * @return
	 */
	public  boolean  addData(User user)
	{
	   	
		ContentValues values = new ContentValues();
		values.put(User.NAME, user.getName());
		values.put(User.MOBLIE, user.getMoblie());
		values.put(User.DANWEI, user.getDanwei());
		values.put(User.QQ, user.getQq());
		values.put(User.ADDRESS, user.getAddress());
	    return	db.save(TABLENAME, values);
	}
	
	/**
	* 获取联系人表数据
	* @return
	*/
	public  User[] getAllUser()
	{
	    Vector<User> v = new Vector<User>();
        Cursor cursor = null;
        try {
            cursor = db.find("select * from " + TABLENAME , null);
          while (cursor.moveToNext()) {
        	User temp = new User();
            temp.setId_DB(cursor.getInt(
            	cursor.getColumnIndex("id_DB")));
            temp.setName(cursor.getString(
            	cursor.getColumnIndex(User.NAME)));
            temp.setMoblie(cursor.getString(
            	cursor.getColumnIndex(User.MOBLIE)));
            temp.setDanwei(cursor.getString(
            	cursor.getColumnIndex(User.DANWEI)));
            temp.setQq(cursor.getString(
                	cursor.getColumnIndex(User.QQ)));
            temp.setAddress(cursor.getString(
                	cursor.getColumnIndex(User.ADDRESS)));
            
            v.add(temp);
          }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.closeConnection();
        }
        if (v.size() > 0) {
            return v.toArray(new User[] {});
        }else
        {
        	User[]  users=new User[1];
        	User  user=new User();
        	user.setName("无结果");
        	users[0]=user;
        	return users;
        }
	}
	/**
	 * 根据数据库变ID主键来获取联系人
	 * @param id
	 * @return
	 */
	public User getUserByID(int id)
	{
		 Cursor cursor = null;
	        try {
	            cursor = db.find(
	            		"select * from " + TABLENAME +"   where  "
	            	+"id_DB=?" , new String[]{id+""});
	        	User temp = new User();
	        	cursor.moveToNext();
	            temp.setId_DB(cursor.getInt(
	            	cursor.getColumnIndex("id_DB")));
	            temp.setName(cursor.getString(
	            	cursor.getColumnIndex(User.NAME)));
	            temp.setMoblie(cursor.getString(
	            	cursor.getColumnIndex(User.MOBLIE)));
	            temp.setDanwei(cursor.getString(
	                 cursor.getColumnIndex(User.DANWEI)));
	            temp.setQq(cursor.getString(
	                 cursor.getColumnIndex(User.QQ)));
	            temp.setAddress(cursor.getString(
	                 cursor.getColumnIndex(User.ADDRESS)));
	            
	           return temp;
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            if (cursor != null) {
	                cursor.close();
	            }
	            db.closeConnection();
	        }
	        return null;
	}
	/**
	* 获取联系人表数据
	* @return
	*/
	public  User[] findUserByKey(String key)
	{
	    Vector<User> v = new Vector<User>();
        Cursor cursor = null;
        try {
            cursor = db.find(
            	"select * from " + TABLENAME +"   where  "
            	+User.NAME+" like '%"+key+"%' " +
            	" or "+User.MOBLIE+" like '%"+key+"%' " +
            	" or "+User.QQ+" like  '%"+key+"%' "
            	, null);
          while (cursor.moveToNext()) {
        	User temp = new User();
            temp.setId_DB(cursor.getInt(
            	cursor.getColumnIndex("id_DB")));
            temp.setName(cursor.getString(
            	cursor.getColumnIndex(User.NAME)));
            temp.setMoblie(cursor.getString(
            	cursor.getColumnIndex(User.MOBLIE)));
            temp.setDanwei(cursor.getString(
                 cursor.getColumnIndex(User.DANWEI)));
            temp.setQq(cursor.getString(
                 cursor.getColumnIndex(User.QQ)));
            temp.setAddress(cursor.getString(
                 cursor.getColumnIndex(User.ADDRESS)));
            v.add(temp);
          }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.closeConnection();
        }
        if (v.size() > 0) {
            return v.toArray(new User[] {});
        }else
        {
        	User[]  users=new User[1];
        	User  user=new User();
        	user.setName("无结果");
        	users[0]=user;
        	return users;
        }
	}
	/**
	 * 修改联系人信息
	 */
	public  boolean updateUser(User user)
	{
	  ContentValues values = new ContentValues();
	  values.put(User.NAME, user.getName());
	  values.put(User.MOBLIE, user.getMoblie());
	  values.put(User.DANWEI, user.getDanwei());
	  values.put(User.QQ, user.getQq());
	  values.put(User.ADDRESS, user.getAddress());
	  return db.update(TABLENAME, 
		     values, "  id_DB=? ", new String[]{user.getId_DB()+""});
	}
	/**
	 * 删除联系人
	 * @param user
	 * @return
	 */
	public  boolean  deleteByUser(User user)
	{
		return db.delete(TABLENAME,
			"  id_DB=?", new String[]{user.getId_DB()+""});
	}
}
