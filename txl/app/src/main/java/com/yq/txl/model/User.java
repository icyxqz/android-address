package com.yq.txl.model;

/**
 * 联系人信息类
 */
public    class User {

		public   final static String  NAME="name";  
		public   final static String  MOBLIE="mobile"; 
		public   final static String  DANWEI="danwei";
		public   final static String  QQ="qq";
		public   final static String  ADDRESS="address";
		private  String name; //用户名
		private  String moblie;//手机号码
		private  String danwei;//单位
		private  String qq;  //QQ
		private  String address;//地址
		private  int id_DB=-1;//数据库主键id


		public int getId_DB() {
			return id_DB;
		}
		public void setId_DB(int idDB) {
			id_DB = idDB;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getMoblie() {
			return moblie;
		}
		public void setMoblie(String moblie) {
			this.moblie = moblie;
		}
		public String getDanwei() {
			return danwei;
		}
		public void setDanwei(String danwei) {
			this.danwei = danwei;
		}
		public String getQq() {
			return qq;
		}
		public void setQq(String qq) {
			this.qq = qq;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}

}
