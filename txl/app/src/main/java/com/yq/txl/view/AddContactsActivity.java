package com.yq.txl.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.yq.txl.R;
import com.yq.txl.dao.ContactsTable;
import com.yq.txl.model.User;

//增加号码记录操作界面
public class AddContactsActivity extends AppCompatActivity {
    /** Called when the activity is first created. */
	//姓名输入框
	private  EditText nameEditText;  
	//手机输入框
	private  EditText mobileEditText;
	//qq
	private  EditText qqEditText;
	//单位
	private  EditText danweiEditText; 
	//地址
	private  EditText addressEditText;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.add);
        setTitle("添加联系人");
        //从已设置的页面布局查找对应的控件
        nameEditText=(EditText)findViewById(R.id.name); 
        mobileEditText=(EditText)findViewById(R.id.mobile); 
        danweiEditText=(EditText)findViewById(R.id.danwei); 
        qqEditText=(EditText)findViewById(R.id.qq); 
        addressEditText=(EditText)findViewById(R.id.address); 
    }


    /**
	 * 创建菜单
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
	     menu.add(Menu.NONE,1, Menu.NONE, "保存");
	     menu.add(Menu.NONE,2, Menu.NONE, "返回");
		 return super.onCreateOptionsMenu(menu);
    }


   /**
    * 菜单事件
    */
	public boolean onOptionsItemSelected(MenuItem item){
		// TODO Auto-generated method stub
		switch (item.getItemId()) { 
        case 1://保存
        	if(!nameEditText.getText().toString().equals(""))
			{
				User user=new User();
				user.setName(nameEditText.getText().toString());
				user.setMoblie(mobileEditText.getText().toString());
				user.setDanwei(danweiEditText.getText().toString());
				user.setQq(qqEditText.getText().toString());
				user.setAddress(addressEditText.getText().toString());
				ContactsTable ct=
					new ContactsTable(AddContactsActivity.this);
				if(ct.addData(user))
				{
					Toast.makeText(AddContactsActivity.this, "添加成功！",
					Toast.LENGTH_SHORT).show();
					finish();
				}else
				{
					Toast.makeText(AddContactsActivity.this, "添加失败！",
					Toast.LENGTH_SHORT).show();
				}
			}else
			{
				Toast.makeText(AddContactsActivity.this, "请先输入数据！",
				Toast.LENGTH_SHORT).show();
			}
         
            break;
        case 2://返回
        	 finish();
        	break;
         default:
             break;
        }
		return super.onOptionsItemSelected(item);
	}


    /**
     * 保存事件
     * @param view
     */
    public void save(View view) {
        if(!nameEditText.getText().toString().equals(""))
        {
            User user=new User();
            user.setName(nameEditText.getText().toString());
            user.setMoblie(mobileEditText.getText().toString());
            user.setDanwei(danweiEditText.getText().toString());
            user.setQq(qqEditText.getText().toString());
            user.setAddress(addressEditText.getText().toString());
            ContactsTable ct=
                    new ContactsTable(AddContactsActivity.this);
            if(ct.addData(user))
            {
                Toast.makeText(AddContactsActivity.this, "添加成功！",
                        Toast.LENGTH_SHORT).show();
                finish();
            }else
            {
                Toast.makeText(AddContactsActivity.this, "添加失败！",
                        Toast.LENGTH_SHORT).show();
            }
        }else
        {
            Toast.makeText(AddContactsActivity.this, "请先输入数据！",
                    Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 取消事件
     * @param view
     */
    public void quxiao(View view) {
        finish();
    }
}