package com.yq.txl.view;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.yq.txl.R;
import com.yq.txl.dao.ContactsTable;
import com.yq.txl.model.User;

/******************** (C) COPYRIGHT 2012********************
*显示联系人界面 
************************************************************/
public class ContactsMessageActivity extends AppCompatActivity {
    /** Called when the activity is first created. */
	//姓名输入框
	private  TextView nameTextView;  
	//手机输入框
	private  TextView mobileTextView;
	//qq
	private  TextView qqTextView;
	//单位
	private  TextView danweiTextView;
	//地址
	private  TextView addressTextView;
	//修改的联系人
	private User user;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.message);
        setTitle("联系人信息");
        
        //从已设置的页面布局查找对应的控件
        nameTextView=(TextView)findViewById(R.id.name); 
        mobileTextView=(TextView)findViewById(R.id.mobile); 
        danweiTextView=(TextView)findViewById(R.id.danwei); 
        qqTextView=(TextView)findViewById(R.id.qq); 
        addressTextView=(TextView)findViewById(R.id.address); 

        //将要修改的联系人数据付值到用户界面显示
        Bundle localBundle = getIntent().getExtras();
        int id=localBundle.getInt("user_ID");
        ContactsTable ct=new ContactsTable(this);
        user =ct.getUserByID(id);
        nameTextView.setText("姓名:"+user.getName());
        mobileTextView.setText("电话:"+user.getMoblie());
        qqTextView.setText("QQ:"+user.getQq());
        danweiTextView.setText("单位:"+user.getDanwei());
        addressTextView.setText("地址:"+user.getAddress());
    }
    /**
	 * 创建菜单
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
	     menu.add(Menu.NONE, 1, Menu.NONE, "返回");
		 return super.onCreateOptionsMenu(menu);
    }
   /**
    * 菜单事件
    */
	public boolean onOptionsItemSelected(MenuItem item){
		// TODO Auto-generated method stub
		switch (item.getItemId()) { 
        case 1://返回
           finish();
           break;
         default:
             break;
        }
		return super.onOptionsItemSelected(item);
	}
}