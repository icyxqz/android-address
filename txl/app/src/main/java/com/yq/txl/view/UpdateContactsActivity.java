package com.yq.txl.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.yq.txl.R;
import com.yq.txl.dao.ContactsTable;
import com.yq.txl.model.User;

/******************** (C) COPYRIGHT 2012********************
*修改号码记录操作界面													
************************************************************/
public class UpdateContactsActivity extends AppCompatActivity {
    /** Called when the activity is first created. */
	//姓名输入框
	private  EditText nameEditText;  
	//手机输入框
	private  EditText mobileEditText;
	//qq
	private  EditText qqEditText;
	//单位
	private  EditText danweiEditText;
	//地址
	private  EditText addressEditText;
	//修改的联系人
	private User user;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.edit);
        setTitle("修改联系人");
        
        //从已设置的页面布局查找对应的控件
        nameEditText=(EditText)findViewById(R.id.name); 
        mobileEditText=(EditText)findViewById(R.id.mobile); 
        danweiEditText=(EditText)findViewById(R.id.danwei); 
        qqEditText=(EditText)findViewById(R.id.qq); 
        addressEditText=(EditText)findViewById(R.id.address); 

        //将要修改的联系人数据赋值到用户界面显示
        Bundle localBundle = getIntent().getExtras();
        int id=localBundle.getInt("user_ID");
        ContactsTable ct=new ContactsTable(this);
        user =ct.getUserByID(id);
        nameEditText.setText(user.getName());
        mobileEditText.setText(user.getMoblie());
        qqEditText.setText(user.getQq());
        danweiEditText.setText(user.getDanwei());
        addressEditText.setText(user.getAddress());
    }
    /**
	 * 创建菜单
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
	     menu.add(Menu.NONE, 1, Menu.NONE, "保存");
	     menu.add(Menu.NONE, 2, Menu.NONE, "返回");
		 return super.onCreateOptionsMenu(menu);
    }
   /**
    * 菜单事件
    */
	public boolean onOptionsItemSelected(MenuItem item){
		// TODO Auto-generated method stub
		switch (item.getItemId()) { 
        case 1://保存
        	if(!nameEditText.getText().toString().equals(""))
    	    {
        		user.setName(nameEditText.getText().toString());
        		user.setMoblie(mobileEditText.getText().toString());
        		user.setDanwei(danweiEditText.getText().toString());
        		user.setQq(qqEditText.getText().toString());
        		user.setAddress(addressEditText.getText().toString());
        		ContactsTable ct=
    					new ContactsTable(UpdateContactsActivity.this);
    				//修改数据库联系人信息
    			if(ct.updateUser(user))
    			{
    				Toast.makeText(UpdateContactsActivity.this, "修改成功！", 
    				Toast.LENGTH_SHORT).show();
    			}else
    			{
    				Toast.makeText(UpdateContactsActivity.this, "修改失败！",
    				Toast.LENGTH_SHORT).show();
    			}
    		}else
    		{
    			Toast.makeText(UpdateContactsActivity.this, "数据不能为空！",
    			Toast.LENGTH_SHORT).show();
    		}
        	break;
        case 2://返回
            finish();
            break;
         default:
             break;
        }
		return super.onOptionsItemSelected(item);
	}

	/**修改按钮
	 * @param view
	 */
	public void update(View view) {
		if(!nameEditText.getText().toString().equals(""))
		{
			user.setName(nameEditText.getText().toString());
			user.setMoblie(mobileEditText.getText().toString());
			user.setDanwei(danweiEditText.getText().toString());
			user.setQq(qqEditText.getText().toString());
			user.setAddress(addressEditText.getText().toString());
			ContactsTable ct=
					new ContactsTable(UpdateContactsActivity.this);
			//修改数据库联系人信息
			if(ct.updateUser(user))
			{
				Toast.makeText(UpdateContactsActivity.this, "修改成功！",
						Toast.LENGTH_SHORT).show();
				finish();
			}else
			{
				Toast.makeText(UpdateContactsActivity.this, "修改失败！",
						Toast.LENGTH_SHORT).show();
			}
		}else
		{
			Toast.makeText(UpdateContactsActivity.this, "数据不能为空！",
					Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * 取消
	 * @param view
	 */
	public void quxiao(View view) {
		finish();
	}
}